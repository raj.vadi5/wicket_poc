package com.wicket_poc.helloworld;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

/**
 * @author VADI on 04-08-2021
 * @project wicket_poc
 */
public class HelloWorld extends WebPage {
    public HelloWorld(){
        add(new Label("message","Hello, World!"));
    }
}
