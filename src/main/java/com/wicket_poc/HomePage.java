package com.wicket_poc;

import com.wicket_poc.helloworld.HelloWorld;
import com.wicket_poc.link.LinkCounter;
import com.wicket_poc.link.LinkCounterWithAJAX;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

/**
 * @author VADI on 03-08-2021
 * @project wicket_poc
 */
public class HomePage extends WebPage {

    public HomePage() {
        /**
         * Link for HelloWorld Example
         */
        add(new Link("helloWorldExample"){
            @Override
            public void onClick() {
                setResponsePage(HelloWorld.class);
            }
        });

        /**
         * Link for Link Counter Example
         */
        add(new Link("linkCounterExample"){
            @Override
            public void onClick() {
                setResponsePage(LinkCounter.class);
            }
        });

        /**
         * Link for Link Counter With AJAX Example
         */
        add(new Link("linkCounterWithAJAXExample"){
            @Override
            public void onClick() {
                setResponsePage(LinkCounterWithAJAX.class);
            }
        });
    }

}
