package com.wicket_poc.link;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

/**
 * @author VADI on 04-08-2021
 * @project wicket_poc
 */
public class LinkCounter extends WebPage {

    private int counter=0; // Initial count value

    public LinkCounter(){
        add(new Link("linkIncrementor"){
            @Override
            public void onClick() {
                counter++;
            }
        });

        /**
         * The PropertyModel is used to access a particular property (in this case it is counter) of its
         *  associated model object at runtime.
         *  PropertyModel instances are dynamic
         */
        add(new Label("linkCounter",new PropertyModel<>(this,"counter")));

        add(new Link("linkCountReset"){
            @Override
            public void onClick() {
                counter=0;
            }
        });
    }
}
