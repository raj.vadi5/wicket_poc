package com.wicket_poc.link;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;

/**
 * @author VADI on 04-08-2021
 * @project wicket_poc
 */
public class LinkCounterWithAJAX extends WebPage {
    private int ajaxCounter=0;
    private Label label;

    public LinkCounterWithAJAX(){
        /**
         * AjaxFallbackLink is a wicket component that works in browsers with and without JS support.
         * When JS is available, AjaxFallbackLink uses Ajax to update specified components on the page.
         * if JS is unavailable, it uses and ordinary web request just like a normal link,
         * updating the whole page.
         */
        add(new AjaxFallbackLink("linkIncrementorAJAX") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                ajaxCounter++;
                if(target != null){
                    target.add(label);
                }
            }
        });
        label = new Label("counts",new PropertyModel(this,"ajaxCounter"));
        label.setOutputMarkupId(true);
        add(label);

        add(new AjaxFallbackLink("linkCountResetAJAX") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                ajaxCounter=0;
                if(target != null){
                    target.add(label);
                }
            }
        });
    }
}
