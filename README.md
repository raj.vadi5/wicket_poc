# Wicket Understanding 
## What problems does wicket solve?
- Wicket bridges the impedance mismatch between stateless HTTP and stateful server-side programming language in java.
- Wicket is like a desktop application and is all about components that know how to render themselves and "pull" their data from a model.
- Building a web application with wicket for the most part feels like regular java programming.
- In Wicket there are basically 3 things. The HTML mark up, the component (everything from a page to a label) and models.

## Table Contents
* [Part 1](#part-1)
    * [HelloWorld Example](#helloworld-example)
        * [Creating a HTML Page](#creating-a-html-page)
        * [Creating a Java class file](#creating-a-java-class-file)

## Part 1
  -  ## HelloWorld Example
        - First, we will display the “Hello, World!” in a browser and the text will be delivered by Wicket. 
        Note: In Wicket each page consists of HTML file and an associated Java class file. By default, both should be           residing in same package folder, but this can be customizable.

        | Example |
        | ------ |        
        | src/wicket/demo/HelloWorld/HelloWorld.java |
        | src/wicket/demo/HelloWorld/HelloWorld.html |
        

     - ## Creating a HTML Page
            <html>
                <body>
                    <h1 wicket:id="message">[text goes here]</h1> <!-- Dynamic part goes here -- >
                </body>
            </html>
        
     - ## Creating a Java class file
            import org.apache.wicket.markup.html.WebPage;
            public class HelloWorld extends WebPage {
                public HelloWorld() { //Constructor
                    /**
                    * @param “message” is identifier 
                    * @param “Hello, World!” is the model
                    **/	
                    add(new Label("message", "Hello, World!")); 
                }
            }

 




